#!/usr/bin/env python
import os
import shutil
from pathlib import Path

import requests
from satpy import Scene

EARTHDATA_USERNAME = os.environ['EARTHDATA_USERNAME']
EARTHDATA_PASSWORD = os.environ['EARTHDATA_PASSWORD']
DATA_DIR = Path('/mnt/nfs-test/data-here')
# If these URLs don't work, query CMR for the correct ones... this example doesn't
# include CMR querying to keep it _simple_. NOTE: Swath first, then geolog granule.
DATA_URLS = [
    'https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/61/MYD02QKM/2021/073/MYD02QKM.A2021073.1415.061.2021074162631.hdf',
    'https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/61/MYD03/2021/073/MYD03.A2021073.1415.061.2021074153143.hdf',
]
DATA_OUTPUT_FPS = [
    DATA_DIR / url.split('/')[-1]
    for url in DATA_URLS
]


class SessionWithHeaderRedirection(requests.Session):
    """Avoid leaking credentials to non-URS hosts.

    This code was provided by NASA:

        https://urs.earthdata.nasa.gov/documentation/for_users/data_access/python
    """
    AUTH_HOST = 'urs.earthdata.nasa.gov'

    def __init__(self, username=EARTHDATA_USERNAME, password=EARTHDATA_PASSWORD):
        super().__init__()
        self.auth = (username, password)

    def rebuild_auth(self, prepared_request, response) -> None:
        """Avoid sending credentials to the wrong host.

            https://tedboy.github.io/requests/generated/generated/requests.Session.rebuild_auth.html
        """
        headers = prepared_request.headers
        url = prepared_request.url

        if 'Authorization' in headers:
            original_parsed = requests.utils.urlparse(response.request.url)
            redirect_parsed = requests.utils.urlparse(url)

            if (
                (original_parsed.hostname != redirect_parsed.hostname)
                and redirect_parsed.hostname != self.AUTH_HOST
                and original_parsed.hostname != self.AUTH_HOST
            ):
                del headers['Authorization']

        return


def download_data() -> None:
    """Download files at `DATA_URLS` into `DATA_DIR`."""
    DATA_DIR.mkdir(exist_ok=True)

    session = SessionWithHeaderRedirection()

    for url in DATA_URLS:
        response = session.get(url, stream=True)
        filepath = DATA_DIR / url.split('/')[-1]

        with open(filepath, 'wb') as f:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)


def remove_data() -> None:
    """Remove files in `DATA_DIR`."""
    for f in DATA_DIR.glob('MYD*'):
        f.unlink()


def remove_data_dir() -> None:
    """Remove `DATA_DIR`.

    If using NFS, this will only succeed if no hidden `.nfs*` files in DATA_DIR are
    open.
    """
    shutil.rmtree(DATA_DIR)


def create_scene() -> Scene:
    """Create a Satpy Scene from data in DATA_DIR."""
    return Scene(
        reader='modis_l1b',
        filenames=[str(path) for path in DATA_OUTPUT_FPS],
    )


def process_scene(scene: Scene) -> None:
    """Pretend some processing is happening in here."""
    # e.g.: scene.save_dataset(..., writer='simple_image', ...)
    return


if __name__ == '__main__':
    # Download two MODIS granules
    download_data()

    # Create a scene with those granules
    scene = create_scene()

    # Do some pretend processing
    process_scene(scene)

    # At this point, `lsof | grep MYD` should show open file handles! This is expected,
    # you might want to load more data from the files.
    # breakpoint()

    remove_data()

    # At this point, `lsof | grep MYD` should still show open files, even though we
    # just deleted them. If we're on NFS, then this deletion actually renamed the files
    # to `.nfs<something>`.
    # breakpoint()

    ###################################################################################
    # This line will trigger:
    #     OSError: [Errno 16] Device or resource busy: '.nfs00000000000018990000000a'
    # 
    # Comment this line to continue running the code... this will trigger a hack to
    # close the files using the garbage-collector then try again.
    remove_data_dir()
    ###################################################################################

    # It's also unexpected that this seems to be
    # (https://github.com/pytroll/satpy/issues/1889) the only way to close the files:
    # How about `scene.close()` or a context manager for a Scene?
    del scene
    import gc; gc.collect()

    # At this point, `lsof | grep MYD` should show no open file handles.
    # breakpoint()

    # Now, we can successfully remove DATA_DIR
    remove_data_dir()
