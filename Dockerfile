FROM continuumio/miniconda3:4.10.3

WORKDIR /opt/workdir

COPY ./environment.yml .

# Should really create a new environment, but this is simple enough...
RUN conda env update --name base -f environment.yml

COPY ./test.py .

CMD ["./test.py"]
