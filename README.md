# About

[What is an SSCCE?](sscce.org)

[Upstream project GitHub issue](https://github.com/pytroll/satpy/issues/2092)

This project demonstrates an issue with the way `satpy` holds file handles open without
offering an interface for closing them. The [issue is
known on GitHub](https://github.com/pytroll/satpy/issues/1889) but not acknowledged to
impact Linux users at this time. The problem arises when using an NFS-mounted filesystem
(and maybe other filesystems or protocols? Does this impact CIFS, for example?).

When you delete an open file on an NFS-mounted filesystem, the NFS client is actually
renaming that file to a file like `.nfs*`, e.g. `.nfs00000000000018990000000a` and
keeping it open. If, after deleting this file, you subsequently try to move or delete
its parent directory, you will receive a message like:

    OSError: [Errno 16] Device or resource busy: '.nfs00000000000018990000000a'

I don't believe `satpy` offers any interface for closing its open files, so in the
above-linked GitHub issue, the recommended solution is:

```
del scene
import gc; gc.collect()
```

And this works. But I believe `satpy` could be improved by creating an explicit
interface (either `scene.close_files()` or a context manager?) so that users who want to
work with `satpy` on an NFS filesystem have a documented way to tell `satpy` "I'm done
working with these files."

I don't know:

* If this impacts all readers, some readers, or just `modis_l1b`.
* If I am (or Google is) really bad at searching and there already exists an interface
  for manually closing files. I'm basing this assumption on lack of Google results and
  the post to the GitHub issue linked at the top of the README.


## Usage

* Export `EARTHDATA_USERNAME` and `EARTHDATA_PASSWORD` with credentials that are
  authorized to access the LAADS DAAC dataproducts. You can visit [this
  URL](https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/61/MYD02QKM/2021/073/MYD02QKM.A2021073.1415.061.2021074162631.hdf)
  to set up or check authorization on your account.
* Follow the Docker or non-Docker instructions below. 
  * Two granules (~200MB) will be downloaded to your chosen NFS mount
  * Then they will be unlinked, causing them to be renamed to `.nfs*` by the NFS client
  * The code will attempt to delete the parent directory of these two deleted granules,
    which will fail with:

        OSError: [Errno 16] Device or resource busy: '.nfs00000000000018990000000a'

### With Docker

* `conda env create` to create the environment with the newest (as of this writing)
  version of `satpy`.
* Mount an NFS share to your desired location and update `docker-compose.yml`'s
  `volumes` section to reflect your chosen location.
* `docker-compose run sscce-satpy-file-handles`


### Without Docker

* Mount an NFS share to `/mnt/nfs-test`
* Run `./test.py`


## Side note: Cryptic error when missing HDF-reading dependency

At first, I forgot to include `pyhdf` in my environment for this SSCCE, and got a
cryptic error:

    ValueError: No supported files found

We came to understand the problem through this GitHub issue:

    https://github.com/pytroll/satpy/issues/338

We would love to see an improved error message, e.g.:

    DependencyError: pyhdf is required to open HDF files. Please add it to your
    environment
